"""Script for demonstrating rounding errors with Numeric types in SQLite."""
import sqlite3


# Connection to the SQLite database
db_connection = sqlite3.connect('temp1.db')
db_cursor = db_connection.cursor()

db_cursor.execute("""
    DROP TABLE IF EXISTS stocks;
""")

# Create the stocks table
db_cursor.execute("""
    CREATE TABLE stocks (
        id INTEGER PRIMARY KEY,
        stock_symbol VARCHAR(10),
        number_of_shares INTEGER,
        purchase_price NUMERIC
    );
""")

# Add records to the stocks table
new_entries = [
    [1, 'AAPL', 16, '406.67890123'],
    [2, 'SBUX', 100, '78.901'],
    [3, 'NFLX', 20, '321.45'],
    [4, 'COST', 26, '498.32']
]
for entry in new_entries:
    db_cursor.execute(f"""
        INSERT INTO stocks(id, stock_symbol, number_of_shares, purchase_price)
        VALUES({entry[0]}, '{entry[1]}', {entry[2]}, '{entry[3]}')
    """)

# Print all the records in the stocks table
db_cursor.execute("""
    SELECT *
    FROM stocks;
""")
print('\nstocks:')
for row in db_cursor.fetchall():
    print(row)

# Print the sum of the stock prices
# Output:
#     Sum of purchase prices: $1305.34990123        # Correct!
db_cursor.execute("""
    SELECT SUM(purchase_price)
    FROM stocks;
""")
print(f'\nSum of purchase prices: ${db_cursor.fetchone()[0]}')

# Print the purchase price multiplied by a scale factor
# to demonstrate rounding errors with the 'Numeric' type in SQLite
# Output:
#     ('AAPL', 1355.582781469959)
#     ('SBUX', 263.0007033)
#     ('NFLX', 1071.4892849999999)   # Rounding Error!  Should be 1071.489285.
#     ('COST', 1661.050056)
db_cursor.execute("""
    SELECT stock_symbol, purchase_price * 3.3333
    FROM stocks;
""")
print('\nStock purchase prices multiplied by 3.3333:')
for row in db_cursor.fetchall():
    print(row)

# Close the database connection
db_connection.commit()
db_connection.close()
